package com.itepem.vibe.domain.enumeration;

/**
 * The RoleType enumeration.
 */
public enum RoleType {
    ADMIN, VIEWER, EDITOR
}
